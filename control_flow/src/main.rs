use std::io;

fn main() {
    let condiditon = true;
    let mut number = if condiditon { 3 } else { 6 };

    if number < 5 {
        println!("Number less then 5");
    } else {
        println!("Number more then 5");
    }

    if number % 4 == 0 {
        println!("Divisiable by 4");
    } else if number % 3 == 0 {
        println!("Divisiable by 3");
    } else if number % 2 == 0 {
        println!("Divisiable by 2");
    } else {
        println!("Not dividable by 4, 3, or 2");
    }

    loop {
        println!("again");
        number += 1;
        if number == 5 { break; }
    }

    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {result}");

    let mut count = 0;
    'counting_up: loop {
        println!("count = {count}");
        let mut remaining = 10;
        
        loop {
            println!("remaining = {remaining}");
            if remaining == 9 {
                break;
            }
            if count == 2 {
                break 'counting_up;
            }
            remaining -= 1;
        }

        count += 1;
    }

    //wrong way
    number = 3;

    while number != 0 {
        println!("{number}!");

        number -= 1;
    }

    println!("BLASTOFF!!!");

    //right way
    for number in (1..4).rev() {
        println!("{number}");
    }

    println!("BLASTOFF!!!");

    //wrong way
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;

    while index < 5 {
        println!("The value is {}", a[index]);

        index += 1;
    }

    //right way
    for element in a {
        println!("The value is: {element}");
    }

    println!("Please give a temprature in Fahrenheit");
    let mut temprature_fahrenheit = String::new();

    io::stdin()
        .read_line(&mut temprature_fahrenheit)
        .expect("Failed to read line");

    let temprature_fahrenheit: f64 = match temprature_fahrenheit.trim().parse() {
        Ok(num) => num,
        Err(_) => 0.0,
    };

    let temprature_celsius = {
        let mut x = temprature_fahrenheit - 32.0;
        x = x * 5.0;
        x / 9.0
    };

    println!("That is {temprature_celsius} in Celsius.");

    println!("How many Fibonacci numbers do you want?");
    let mut numbers = String::new();

    io::stdin()
        .read_line(&mut numbers)
        .expect("Failed to read line");

    let numbers: u32 = match numbers.trim().parse() {
        Ok(num) => num,
        Err(_) => 0,
    };

    let mut first = 0;
    let mut second = 1;
    println!("The first 2 Fibonacci numbers are {first} and {second}.");

    for n in 2..numbers {
        let temp: u32 = second;
        second = first + second;
        first = temp;

        if n + 1 == 3 {
            println!("The 3rd Fibonacci number is: {}", second);
            continue;
        }

        println!("The {}th Fibonacci number is: {}", n + 1, second);
    }

}
