use std::io;

fn main() {
    let _guess: u32 = "42".parse().expect("Not a number!");

    let _x = 2.0; // f64

    let _y: f32 = 3.0; // f32

    let _sum = 5 + 10;

    let _difference = 95.5 - 4.3;

    let _product = 4 * 30;

    let _quotient = 56.7 / 34.2;
    let _truncated = -5 / 3;

    let _remainder = 43 % 5;

    let _t = true;

    let _f: bool = false;

    let _c = 'z';
    let _z: char = 'b';

    let tup: (i32, u32, f64) = (-5, 5, 5.0);

    let (_x, y, _z) = tup;

    println!("The value of y is: {y}");
    
    let _b = tup.0;

    let a: [u64; 6] = [1,2,3,4,5,6];

    let _s = [3; 5];

    let _first = a[0];

    println!("Please enter an array index");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");
    
    let index: usize = index
        .trim()
        .parse()
        .expect("NAN");

    let element = a[index];

    println!("The value of the element at index {index} is: {element}");
    
}
