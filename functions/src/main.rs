fn main() {

    //statement
    let y = 6;

    //statement with expression
    let z = {
        let x = 3;
        x + 1
    };

    let w = five();

    let a = plus_one(35);

    println!("Hello, world!");

    println!("The value of z is: {z}");

    println!("The value of w is {w}");

    println!("The value of a is {a}");

    another_function();

    parameters(y);

    print_labeled_mesuremnts(543, 'F');
}

fn another_function() {
    println!("Another function.")
}

fn parameters(number: i64) {
    println!("The value of number is: {number}")
}

fn print_labeled_mesuremnts(value: i64, unit_lable: char) {
    println!("The measurement is {value}{unit_lable}");
}

fn five() -> i32 {
    5
}

fn plus_one(x: i32) -> i32 {
    x + 1
}
