fn read(y: bool) {
    if y {
        println!("y is true");
    }
}

fn main() {
    let a = Box::new([0; 1_000_000]);
    let _b = a; //wastes a lot of memory without pointers using Box::new();

    let x = true; //safe
    read(x);
    //let x = true; //unsafe
}
